###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build Mouse `n Cat program
###
### @author  Paulo Baldovi <pbaldovi@hawaii.edu>
### @date    25 Jan 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = g++
CFLAGS = -g -Wall

TARGET = mouseNcat

all: $(TARGET)

mouseNcat: mouseNcat.cpp
	$(CC) $(CFLAGS) -o $(TARGET) mouseNcat.cpp

clean:
	rm -f $(TARGET) *.o

